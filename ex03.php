<?php
	class Diagram{
		public $layers = array();
        
        //Add one layer to the diagram
        public function addLayer($obj){
            $this->layers[] = $obj;
        }
        
        //Delete layer of $key
        public function deleteLayer($key){
            if (isset($this->layers[$key])){
                unset($this->layers[$key]);
            }
            else {
                echo("Invalid key $key");
            }
        }
        
        public function getLayer($key){
            if (isset($this->layers[$key])){
                return $this->layers[$key];
            }
            else {
                echo("Invalid key $key");
            }
        }
        
        public function deleteRectangle(){
            foreach ($this->layers as $key => $layer){
                foreach($layer->shapes as $key => $shape){
                    if(($key = array_search($shape, $layer->shapes, TRUE)) !== FALSE) {
                        unset($layer->shapes[$key]);
                    }
                }            
            }
        }
	}	
    
    class Layer{
        public $shapes = array();
        
        public function addShape($obj){
            $this->shapes[] = $obj;
        }
        
        public function deleteShape($key){
            if (isset($this->shapes[$key])){
                unset($this->shapes[$key]);
            }
            else {
                throw new KeyInvalidException("Invalid key $key");
            }
        }
        
        public function getShape($key){
            if (isset($this->shapes[$key])){
                return $this->shapes[$key];
            }
            else {
                throw new KeyInvalidException("Invalid key $key");
            }
        }
        
        public function deleteRectangle(){
            foreach ($this->shapes as $key => $shape){
                if($shape instanceof Rectangle){
                    if(($key = array_search($shape, $this->shapes, TRUE)) !== FALSE) {
                        unset($this->shapes[$key]);
                    }
                }              
            }
        }
        
        public function showRectangle(){
            foreach ($this->shapes as $key => $shape){
                if($shape instanceof Rectangle){
                    echo "Size: " . $shape->size . "<BR>";
                }              
            }
        }
        
        public function showCircle(){
            foreach ($this->shapes as $key => $shape){
                if($shape instanceof Circle){
                    echo "Size: " . $shape->size . "<BR>";
                }              
            }
        }
    }
    
    class Shape{
        public $position_X;
        public $position_Y;
        public $size;
    }
    
    class Rectangle extends Shape{
        public $height;
        public $width;
        public function __construct($height, $width, $position_X, $position_Y){
            $this->height = $height;
            $this->width = $width;
            $this->size = $height * $width;
            $this->position_X = $position_X;
            $this->position_Y = $position_Y;
        }
    }

    class Circle extends Shape{
        public $radius;

        public function __construct($radius, $position_X, $position_Y){
            $this->radius = $radius;
            $this->size = 3.14*$radius*$radius;
            $this->position_X = $position_X;
            $this->position_Y = $position_Y;
        }
    }
    
    $diagram = new Diagram();
    $layer1 = new Layer();
    $layer2 = new Layer();
    $diagram->addLayer($layer1);
    $diagram->addLayer($layer2);
    
    $rectangle1 = new Rectangle(100,300,60,800);
    $rectangle2 = new Rectangle(50,680,180,400);
    $circle1 = new Circle(100,100,600);
    $layer1->addShape($rectangle1);
    $layer1->addShape($rectangle2);
    $layer1->showRectangle();
    $diagram->deleteRectangle();
    $layer1->showRectangle();
    $layer2->addShape($circle1);
?>